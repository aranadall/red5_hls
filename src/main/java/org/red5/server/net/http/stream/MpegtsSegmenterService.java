package org.red5.server.net.http.stream;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.apache.mina.core.buffer.IoBuffer;
import org.red5.codec.IAudioStreamCodec;
import org.red5.codec.IStreamCodecInfo;
import org.red5.codec.IVideoStreamCodec;
import org.red5.conf.ExtConfiguration;
import org.red5.io.ts.FLV2MPEGTSChunkWriter;
import org.red5.io.utils.HexDump;
import org.red5.server.api.scope.IScope;
import org.red5.server.api.stream.IBroadcastStream;
import org.red5.server.api.stream.IStreamListener;
import org.red5.server.api.stream.IStreamPacket;
import org.red5.server.net.rtmp.event.AudioData;
import org.red5.server.net.rtmp.event.IRTMPEvent;
import org.red5.server.net.rtmp.event.VideoData;
import org.red5.server.net.rtmp.event.VideoData.FrameType;

import lombok.extern.slf4j.Slf4j;

/**
 * FLV TO Mpeg2ts Segmenter
 * @author pengliren
 * 
 *	mpeg2ts 的详细介绍 https://blog.csdn.net/Kayson12345/article/details/81266587
 */
@Slf4j
public class MpegtsSegmenterService implements IStreamListener {
 
	private static ConcurrentMap<String, ConcurrentHashMap<String, SegmentFacade>> scopeSegMap = new ConcurrentHashMap<String, ConcurrentHashMap<String,SegmentFacade>>();
	/*切片的时长 5秒钟*/
	private long segmentTimeLimit = ExtConfiguration.HLS_SEGMENT_TIME * 1000;
	/*提前 最大的 切片的长度 */
	private int maxSegmentsPerFacade = ExtConfiguration.HLS_SEGMENT_MAX;
	/* 单例模式  */
	private static final class SingletonHolder {
		private static final MpegtsSegmenterService INSTANCE = new MpegtsSegmenterService();
	}
	
	protected MpegtsSegmenterService() {}
	
	public static MpegtsSegmenterService getInstance() { 
		return SingletonHolder.INSTANCE;
	} 
	
	/**
	 * 根据应用名和流名称获取切片个数
	 * @param scopeName
	 * @param streamName
	 * @return int
	 */
	public int getSegmentCount(String scopeName, String streamName) {
		ConcurrentHashMap<String, SegmentFacade> segments = scopeSegMap.get(scopeName);
		if (segments.containsKey(streamName)) {
			SegmentFacade facade = segments.get(streamName);
			return facade.getSegmentCount();
		} else {
			return 0;
		}   	
    }	
	/**
	 * 根据应用名和流名称获取切片
	 * @param scopeName
	 * @param streamName
	 * @return MpegtsSegment
	 */
	public MpegtsSegment getSegment(String scopeName, String streamName) {
		ConcurrentHashMap<String, SegmentFacade> segments = scopeSegMap.get(scopeName);
		if (segments.containsKey(streamName)) {
			SegmentFacade facade = segments.get(streamName);
			return facade.getSegment();
		} else {
			return null;
		}
	}
	/**
	 * 根据应用名、流名称和索引id获取切片
	 * @param scopeName
	 * @param streamName
	 * @return MpegtsSegment
	 */
	public MpegtsSegment getSegment(String scopeName, String streamName, int index) {
		ConcurrentHashMap<String, SegmentFacade> segments = scopeSegMap.get(scopeName);
		if (segments.containsKey(streamName)) {
			SegmentFacade facade = segments.get(streamName);
			return facade.getSegment(index);
		} else {
			return null;
		}
	}
	/**
	 * 根据应用名和流名称获取全部切片
	 * @param scopeName
	 * @param streamName
	 * @return List<MpegtsSegment>
	 */ 
	public List<MpegtsSegment> getSegmentList(String scopeName, String streamName) {
		ConcurrentHashMap<String, SegmentFacade> segments = scopeSegMap.get(scopeName);
		if (segments.containsKey(streamName)) {
			SegmentFacade facade = segments.get(streamName);
			return facade.segments;
		} else {
			return null;
		}
	}
	/**
	 * 应用是否有效
	 * @param scope 		作用域
	 * @param streamName    流名称
	 * @return boolean
	 */
	public boolean isAvailable(IScope scope, String streamName) {
		
		ConcurrentHashMap<String, SegmentFacade> segments = scopeSegMap.get(scope.getName());

		if (segments == null) {
			segments = new ConcurrentHashMap<String, MpegtsSegmenterService.SegmentFacade>();
			scopeSegMap.put(scope.getName(), segments);
		} 
		return segments.containsKey(streamName);
	}
	 
	/**
	 * 根据应用和流来更新对应的切片
	 * @param stream	流
	 * @param scope     作用域
	 * @param name      作用于名称
	 * @param event		命令消息体
	 */
	public void update(IBroadcastStream stream, IScope scope, String name, IRTMPEvent event) {
		ConcurrentHashMap<String, SegmentFacade> segments = scopeSegMap.get(scope.getName());
		if (segments == null) {
			segments = new ConcurrentHashMap<String, MpegtsSegmenterService.SegmentFacade>();
			scopeSegMap.put(scope.getName(), segments);
		}
		SegmentFacade facade = segments.get(name);
		if (facade == null) { //http live stream aes 128是否加密在这里处理 
			facade = new SegmentFacade(name, ExtConfiguration.HLS_ENCRYPT);
			segments.put(name, facade);
		}
		try {
			facade.writeEvent(stream, event);
		} catch (IOException e) {
			log.info("write ts exception {}", e.getMessage());
		}
	}
	
	/**
	 * 获取切片密匙key
	 * @param scopeName
	 * @param streamName
	 * @return String
	 */
	public String getSegmentEnckey(String scopeName, String streamName) {
		String encKey = null;
		ConcurrentHashMap<String, SegmentFacade> segments = scopeSegMap.get(scopeName);	
		if(segments != null) {
			SegmentFacade facade = segments.get(streamName);
			if(facade != null) encKey = facade.encKey;
		}
		return encKey;
	}
	
	/**
	 * 切片是否加密
	 * @param scopeName
	 * @param streamName
	 * @return boolean
	 */
	public boolean getSegmentIsEncrypt(String scopeName, String streamName) {
		boolean isEncrypt = false;
		ConcurrentHashMap<String, SegmentFacade> segments = scopeSegMap.get(scopeName);	
		if(segments != null) {
			SegmentFacade facade = segments.get(streamName);
			if(facade != null) isEncrypt = facade.isEncrypt;
		}
		return isEncrypt;
	}
	
	/**
	 * 删除切片
	 * @param scopeName
	 * @param streamName
	 */
	public void removeSegment(String scopeName, String streamName) {
	
		ConcurrentHashMap<String, SegmentFacade> segments = scopeSegMap.get(scopeName);	
		if(segments != null) {
			SegmentFacade facade = segments.remove(streamName);
			if(facade != null) facade.close();
		}
	}
	
	/**
	 * 获取流消息
	 */
	@Override
	public void packetReceived(IBroadcastStream stream, IStreamPacket packet) {
		if(packet instanceof VideoData || packet instanceof AudioData) this.update(stream, stream.getScope(), stream.getPublishedName(), (IRTMPEvent)packet);
	}
	
	/**
	 * 切片实体类
	 * @author Administrator
	 *
	 */
	private class SegmentFacade {
		/*全部切片*/
		List<MpegtsSegment> segments = new ArrayList<MpegtsSegment>();		
		/* 临时切片，当前流要写入的切片 */
		// segment currently being written to
		MpegtsSegment segment;
		/* 切片索引 */
		// segment index counter
		AtomicInteger counter = new AtomicInteger();
		/* 视频和音频数 */
		// video and audio packet count
		AtomicInteger frameCounter = new AtomicInteger();
		/* 是否加密  */
		boolean isEncrypt = false;
		/* 密匙key */
		String encKey;
		
		/* flv转mpegts块写入类 */
		FLV2MPEGTSChunkWriter writer;
		
		String streamName;
		long startTimeStamp = -1L;
		long lastTimeStamp = 0;
		IoBuffer videoConfig;
		IoBuffer audioConfig;
		
		SegmentFacade(String streamName, boolean isEncrypt) {
			this.isEncrypt = isEncrypt;
			this.streamName = streamName;
			if (isEncrypt) {
				this.encKey = generatKey();
				log.info("http live stream publish, name : {}, is encrypt, enc key : {}", streamName, encKey);
			} else {
				log.info("http live stream publish : {}", streamName);
			}
		}
		
		public int getSegmentCount() {
			return segments.size();
		}
		
		public MpegtsSegment getSegment() {
			return segment;
		}
		
		public MpegtsSegment getSegment(int index) {

			for (MpegtsSegment segment : segments) {
				if (segment.getSequence() == index)
					return segment;
			}
			return null;
		}
		
		public void close() {
			writer = null;
			segments.clear();
			segment = null;
			videoConfig = null;
			audioConfig = null;
			log.info("http live stream unpublish, name : {}", streamName);
		}
		
		public void  writeEvent(IBroadcastStream stream, IRTMPEvent event) throws IOException{	
			// fix wait video and audio config 
			if(frameCounter.get() <= 20) {
				frameCounter.incrementAndGet();
				return;
			}
			
			if(startTimeStamp == -1L) startTimeStamp = event.getTimestamp(); 
			
			boolean newSegment = false;
			
			if (event.getTimestamp() > lastTimeStamp) {
				lastTimeStamp = event.getTimestamp();
			}
			
			if (segment == null) {
				if(event instanceof VideoData && ((VideoData) event).getFrameType() == FrameType.KEYFRAME) {
					segment = new MpegtsSegment(streamName, counter.incrementAndGet());
					segment.setEncKey(encKey);
					// flag that we created a new segment
					newSegment = true;
				} else {
					// first segment waiting video keyframe
					return;
				}
			} else {
				long currentSegmentTs = event.getTimestamp() - startTimeStamp; 
				if ((currentSegmentTs >= segmentTimeLimit) && event instanceof VideoData && ((VideoData) event).getFrameType() == FrameType.KEYFRAME) {
					
					writer.endChunkTS();
					// close active segment
					segment.close();
					segments.add(segment);
					startTimeStamp = event.getTimestamp();
					// create a segment
					segment = new MpegtsSegment(streamName, counter.incrementAndGet());
					segment.setEncKey(encKey);
					newSegment = true;
				}
			}
			
			if (newSegment) {				
				if (segments.size() > maxSegmentsPerFacade) {
					// get current segments index minux max
					int rmNum = segments.size() - maxSegmentsPerFacade;
					MpegtsSegment seg = null;
					if(rmNum > 0) {
						for(int i = 0; i < rmNum; i++) {
							seg = segments.remove(0);
							seg.dispose();
						}
					}					
				}
				
				// 音视频config信息
				if (videoConfig == null || audioConfig == null) {
					IStreamCodecInfo codecInfo = stream.getCodecInfo();
					IVideoStreamCodec videoCodecInfo = null;
					if (codecInfo != null && codecInfo.hasVideo()) {
						videoCodecInfo = codecInfo.getVideoCodec();
					}

					if (videoCodecInfo != null && videoCodecInfo.getDecoderConfiguration() != null) {
						videoConfig = videoCodecInfo.getDecoderConfiguration().asReadOnlyBuffer();
					}

					IAudioStreamCodec audioCodecInfo = null;
					if (codecInfo != null && codecInfo.hasAudio()) {
						audioCodecInfo = codecInfo.getAudioCodec();
					}

					if (audioCodecInfo != null && audioCodecInfo.getDecoderConfiguration() != null) {
						audioConfig = audioCodecInfo.getDecoderConfiguration().asReadOnlyBuffer();
					}
				}
								
				if (writer == null) {
					writer = new FLV2MPEGTSChunkWriter(videoConfig, audioConfig, isEncrypt);
				}
				
				writer.startChunkTS(segment);
			}
			writer.writeStreamEvent(event);
		}
	}
	
	public static String generatKey() {

		KeyGenerator kgen;
		String encKey = null;
		try {
			kgen = KeyGenerator.getInstance("AES");
			kgen.init(128);
			SecretKey skey = kgen.generateKey();
			byte[] raw = skey.getEncoded();
			encKey = HexDump.encodeHexString(raw);
		} catch (NoSuchAlgorithmException e) {
			log.error("generat hls key fail : {}", e.toString());
		}
		
		return encKey;
	}
	
	public long getSegmentTimeLimit() {
		return segmentTimeLimit;
	}

	public void setSegmentTimeLimit(long segmentTimeLimit) {
		this.segmentTimeLimit = segmentTimeLimit;
	}

	public int getMaxSegmentsPerFacade() {
		return maxSegmentsPerFacade;
	}

	public void setMaxSegmentsPerFacade(int maxSegmentsPerFacade) {
		this.maxSegmentsPerFacade = maxSegmentsPerFacade;
	}
}
